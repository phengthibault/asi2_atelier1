package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.UserModelPublic.UserModelPublic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Message;




@Component
public class BusListener {

    @Autowired
    JmsTemplate jmsTemplate;
    
    @Autowired
    private UserService userService;

/*
    @JmsListener(destination = "USER_BUS", containerFactory = "connectionFactory")
    public void receiveMessageResult(UserModelPublic user, Message message) {

            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String User=["+user+"] MSG=["+message+"]");
            if(userService.getUser(user.getId()) != null ) {
                userService.updateUser(UserModelPublicToUserModel(user));

            }else {
                userService.addUser(UserModelPublicToUserModel(user));
            }

    }
    */
    
    @JmsListener(destination = "USER_BUS", containerFactory = "connectionFactory")
    public void receiveMessageResult(UserModelPublic user) {

            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String User=["+user+"]");
            
            if(userService.getUser(user.getId()) != null ) {
                userService.updateUser(UserModelPublicToUserModel(user));

            }else {
                userService.addUser(UserModelPublicToUserModel(user));
            }
            

    }

	
	public UserModel UserModelPublicToUserModel(UserModelPublic userPublic) {
		return new UserModel(
				userPublic.getId(),
				userPublic.getLogin(),
				userPublic.getPwd(),
				userPublic.getAccount(),
				userPublic.getLastName(),
				userPublic.getSurName(),
				userPublic.getEmail()
				);	
	}

    

}
