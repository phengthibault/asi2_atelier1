package com.cpe.springboot.message.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.message.model.MessageModel;
import com.cpe.springboot.store.model.StoreModel;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	public void saveMessage(MessageModel msg) {
		messageRepository.save(msg);
		System.out.println("Saving message");
		
	}

}
