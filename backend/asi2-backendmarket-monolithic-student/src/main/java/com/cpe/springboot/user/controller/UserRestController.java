package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.card.model.CardLightModel;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.model.UserDisplay;
import com.cpe.springboot.user.model.UserModel;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		return userService.getAllUsers();

	}
	
	@RequestMapping("/user/{id}")
	private UserModel getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public void addUser(@RequestBody UserModel user) {
		System.out.println(	user.getAccount());
		userService.addUser(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public void updateUser(@RequestBody UserModel user,@PathVariable String id) {
		user.setId(Integer.valueOf(id));
		userService.updateUser(user);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}

	
	@RequestMapping(method=RequestMethod.GET,value="/userCards/{id}")
	private  List<CardLightModel> getCardofUser(@PathVariable String id) {
			Optional<UserModel> user=userService.getUser(id);
			 Set<CardModel> listCard=user.get().getCardList();
			System.out.println(listCard.toString());
			List<CardLightModel> cLightList=new ArrayList<>();
			for(CardModel c:listCard){
				cLightList.add(new CardLightModel(c));
			}
			return cLightList;	
		}
	@RequestMapping(method=RequestMethod.GET,value="/userCardslog/{login}")
	private  List<CardLightModel> getCardofUserByLog(@PathVariable String login) {
		UserModel user=userService.getUserByLogin(login).get(0);
			 Set<CardModel> listCard=user.getCardList();
			System.out.println("userlog"+listCard.toString());
			List<CardLightModel> cLightList=new ArrayList<>();
			for(CardModel c:listCard){
				cLightList.add(new CardLightModel(c));
			}
			return cLightList;	
		}
	
	
	@RequestMapping(method=RequestMethod.POST,value="/auth",consumes="application/json")
	private  ResponseEntity<Integer>  getAllCourses( @RequestBody User userBody) {
		String login=userBody.getLogin();
		String pwd=userBody.getPwd();
			System.out.println(login + pwd);

		if( userService.getUserByLoginPwd(login,pwd).size() > 0) {
			UserModel user=userService.getUserByLoginPwd(login,pwd).get(0);
			System.out.println(user.getEmail());
			
			return  new ResponseEntity<>(user.getId(), HttpStatus.OK);
		}
		return  new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	

}
