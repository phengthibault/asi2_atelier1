package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.Controller.CardModelRepository;
import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.Controller.CardRefRepository;
import com.cpe.springboot.card.Controller.CardReferenceService;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CardModelRepository cardModelrepository;
	
	@Autowired
	private CardReferenceService cardRefService;

	@Autowired
	private CardModelService cardModelService;
	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	
	public void addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		userRepository.save(user);
		//List<CardReference> listCardref=cardRefService.getAllCardRef();
		//System.out.println("nombre de carte ref"+listCardref.size());
		List<CardModel> cardList=cardModelService.getRandCard(10);
		for(CardModel card: cardList) {
			cardModelrepository.save( card);
		}
		
		/*for(CardModel card: cardList) {
			user.addCard(card);
		}*/
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}
	public List<UserModel> getUserByLogin(String login) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLogin(login);
		return ulist;
	}

}
