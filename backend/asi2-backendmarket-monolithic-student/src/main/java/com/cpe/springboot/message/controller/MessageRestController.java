package com.cpe.springboot.message.controller;

import com.cpe.springboot.message.model.MessageModel;
import com.cpe.springboot.store.model.StoreOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class MessageRestController {

	

	@Autowired
	private MessageService messageService;
	
	@RequestMapping(method= RequestMethod.POST,value="/message")
	public void saveMessageInDB (@RequestBody MessageModel msg) {
		System.out.println("in /message");
		messageService.saveMessage(msg);

	}

	
}
