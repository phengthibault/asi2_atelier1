package com.cpe.springboot.card.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.CardReference;

@Service
public class CardReferenceService {


	@Autowired
	private CardRefRepository cardRefRepository;

	public List<CardReference> getAllCardRef() {
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public void addCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);

	}

	public CardReference getRandCardRef() {
		List<CardReference> cardRefList=getAllCardRef();
		if( cardRefList.size()>0) {
			Random rand=new Random();
			int rindex=rand.nextInt(cardRefList.size()-1);
			return cardRefList.get(rindex);
		}
		return null;
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
	 String[] img= {"http://medias.3dvf.com/news/sitegrab/gits2045.jpg","https://picsum.photos/200/300","https://static.hitek.fr/img/actualite/2017/06/27/i_deadpool-2.jpg","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPrLtcQ4MiHtvh3kGecFZk64MRK5jmtDsKCNn9ngaVvTm1Iq9-","https://www.screengeek.net/wp-content/uploads/2018/01/batman-dc-comics.jpg",
			 "https://www.lepoint.fr/images/2018/09/12/16899168lpw-16899216-article-jpg_5564007.jpg","https://static.hitek.fr/img/actualite/2017/10/24/fb_iron-man-illustr.jpg","http://www.premiere.fr/sites/default/files/styles/scale_crop_1280x720/public/2019-10/ScarJoVersDef.jpg",
			"https://cdn.mos.cms.futurecdn.net/Z3NkfR4ffrtfMiJ64786xQ-1200-80.jpg","https://img.gameme.eu/images/EXR5bm2Skv6LGnuPGZXMKg.jpg" };	
		for(int i=0;i<10;i++){
			CardReference cardRef =new CardReference("name"+i,"description"+i,"family"+i,"affinity"+i,img[i],"https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg");
			addCardRef(cardRef);				
		}
		
	}
	
}
