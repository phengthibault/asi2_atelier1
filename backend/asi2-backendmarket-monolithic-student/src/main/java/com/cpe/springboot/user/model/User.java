package com.cpe.springboot.user.model;

public class User {

	private String login;
	private String pwd;
	
    public User(String login,String pwd) {
    	this.pwd=pwd;
    	this.login=login;
    }

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
    

	
}
