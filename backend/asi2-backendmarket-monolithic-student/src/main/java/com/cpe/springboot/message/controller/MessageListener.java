package com.cpe.springboot.message.controller;

import com.cpe.springboot.message.model.MessageModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageListener {

    @Autowired
    JmsTemplate jmsTemplate;
    
    @Autowired
    private MessageService messageService;

    
    @JmsListener(destination = "MSG_BUS", containerFactory = "connectionFactory")
    public void receiveMessageResult(MessageModel msg) {

            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String message=["+msg+"]");
            System.out.println("From : " + msg.getEmitterName()+" to: "+ msg.getReceiverName());
            messageService.saveMessage(msg);           

    }    

}