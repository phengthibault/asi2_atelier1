package com.cpe.springboot.store.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.store.model.StoreModel;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;

@Service
public class StoreService {
	
	@Autowired
	CardModelService cardService;
	@Autowired
	UserService userService; 
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	private CardModelService cardModelService;
	private StoreModel store;
	
	public void generateNewStore(String name, int nb) {
		StoreModel store =new StoreModel();
		store.setName(name);
		
		List<CardModel> cardList=cardService.getRandCard(nb);
		for(CardModel c: cardList) {
			store.addCard(c);
		}
		storeRepository.save(store);
		this.store=store;
	}
	
	public boolean buyCard(Integer user_id, Integer card_id) {

		Optional<UserModel> u_option=userService.getUser(user_id);
		Optional<CardModel> c_option=cardService.getCard(card_id);		
		System.out.println("on est dans le store buy card"+u_option.isPresent()+c_option.isPresent());
		c_option.get().setUser(u_option.get());   //j'ajoute l'id d'user avec la carte 
		if(!u_option.isPresent() || !c_option.isPresent()){
			return false;
		}
		UserModel u=u_option.get();
		CardModel c=c_option.get();
		System.out.println("on est dans le store buy card");
		System.out.println("account user +prix"+u.getAccount()+"prix"+ c.getPrice());

		if(u.getAccount() > c.getPrice()) {
			System.out.println("oui"+u.getAccount()+ c.getPrice());

			u.addCard(c);
			cardModelService.deleteCardModel(card_id);
			u.setAccount(u.getAccount()-c.getPrice());
			userService.updateUser(u);
			return true;
		}else {
			return false;
		}
	}
	
	public boolean sellCard(Integer user_id, Integer card_id) {
		Optional<UserModel> u_option=userService.getUser(user_id);
		Optional<CardModel> c_option=cardService.getCard(card_id);
		if(!u_option.isPresent() || !c_option.isPresent()){
			return false;
		}
		UserModel u=u_option.get();
		CardModel c=c_option.get();

		//c.setStore(this.store);
		c.setUser(null);
		cardService.updateCard(c);
		u.setAccount(u.getAccount()+c.getPrice());
		userService.updateUser(u);
		return true;
	}
	
	public Set<CardModel> getAllStoreCard(){
		return this.store.getCardList();
	}
	
	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
	
				
		}
	
	
}
