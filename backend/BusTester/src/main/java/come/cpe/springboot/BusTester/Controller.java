package come.cpe.springboot.BusTester;

import com.cpe.springboot.UserModelPublic.UserModelPublic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    BusService busService;

    @RequestMapping(method = RequestMethod.POST, value = "/AddUser")
    public boolean AddUser(@RequestBody UserModelPublic user) {
        busService.sendMsg(user);
        return true;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/AddUser/{busName}")
    public boolean sendInform(@RequestBody UserModelPublic user, @PathVariable String busName) {
        busService.sendMsg(user,busName);
        return true;
    }

}
