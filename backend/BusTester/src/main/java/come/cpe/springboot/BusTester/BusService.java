package come.cpe.springboot.BusTester;

import com.cpe.springboot.UserModelPublic.UserModelPublic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class BusService {

	@Autowired
    JmsTemplate jmsTemplate;

    public void sendMsg(UserModelPublic user) {
    	
        System.out.println("[BUSSERVICE] SEND String MSG=["+user+"]");
        jmsTemplate.convertAndSend("USER_BUS",user);
    }

    public void sendMsg(UserModelPublic user, String busName) {
        System.out.println("[BUSSERVICE] SEND String MSG=["+user+"] to Bus=["+busName+"]");
        jmsTemplate.convertAndSend(busName,user);
    }
}
