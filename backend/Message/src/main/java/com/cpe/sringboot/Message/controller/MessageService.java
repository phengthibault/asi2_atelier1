package com.cpe.sringboot.Message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.sringboot.Message.model.MessageModel;


@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	public void saveMessage(MessageModel msg) {
		messageRepository.save(msg);
		System.out.println("Saving message");
		
	}

}
