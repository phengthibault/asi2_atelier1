package com.cpe.sringboot.Message.model;

public class MessageDTO {
	
	private String emitterName;
	private String receiverName;
	private Long date;
	private String textMessage;
	
	public MessageDTO(String emitterName, String receiverName, Long date, String textMessage) {
		this.emitterName = emitterName;
		this.receiverName = receiverName;
		this.date = date;
		this.textMessage = textMessage;
	}
	
	public MessageDTO() {

	}

	public String getEmitterName() {
		return emitterName;
	}

	public void setEmitterName(String emitterName) {
		this.emitterName = emitterName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public String getTextMessage() {
		return textMessage;
	}

	public void setTextMessage(String textMessage) {
		this.textMessage = textMessage;
	}
	
	
}
