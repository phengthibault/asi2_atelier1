package com.cpe.sringboot.Message.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@SuppressWarnings("serial")
@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class MessageModel implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("emitterName")
	private String emitterName;
	
	@JsonProperty("receiverName")
	private String receiverName;
	
	@JsonProperty("date")
	private Long date;
	
	@JsonProperty("textMessage")
	private String textMessage;
	
	public MessageModel(String emitterName, String receiverName, Long date, String textMessage) {
		super();
		this.emitterName = emitterName;
		this.receiverName = receiverName;
		this.date = date;
		this.textMessage = textMessage;
	}
	
	public MessageModel() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmitterName() {
		return emitterName;
	}

	public void setEmitterName(String emitterName) {
		this.emitterName = emitterName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long timestamp) {
		this.date = timestamp;
	}

	public String getTextMsg() {
		return textMessage;
	}

	public void setTextMsg(String textMsg) {
		this.textMessage = textMsg;
	}
	
}



	


	

