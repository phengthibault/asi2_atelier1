package com.cpe.sringboot.Message.controller;
import com.cpe.sringboot.Message.model.MessageModel;

import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<MessageModel, Integer> {
	

}
