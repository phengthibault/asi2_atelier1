package com.cpe.sringboot.Message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.cpe.sringboot.Message.model.MessageDTO;
import com.cpe.sringboot.Message.model.MessageModel;

@Component
public class MessageListener {

    @Autowired
    JmsTemplate jmsTemplate;
    
    @Autowired
    private MessageService messageService;

    
    @JmsListener(destination = "MSG_BUS", containerFactory = "connectionFactory")
    public void receiveMessageResult(MessageDTO msgDTO) {

            System.out.println("[BUSLISTENER] [CHANNEL RESULT_BUS_MNG] RECEIVED String message=["+msgDTO+"]");
            System.out.println("From : " + msgDTO.getEmitterName()+" to: "+ msgDTO.getReceiverName());
            messageService.saveMessage(new MessageModel(msgDTO.getEmitterName(),msgDTO.getReceiverName(),
            		msgDTO.getDate(),msgDTO.getTextMessage()));           

    }    

}