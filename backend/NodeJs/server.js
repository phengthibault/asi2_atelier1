const express = require("express");
const app = express();
const server = require("http").Server(app);
const ioServer = require("socket.io")(server);

const Message = require("./models/Message");

var fs = require("fs");

app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));

app.use(express.static("build"));
app.use(express.static("build/static"));

//list of differents users
const messages = [];

const stompit = require("stompit");
const connectOptions = { host: "localhost", port: 61613 };
const headers = { destination: "/queue/MSG_BUS" };

//list of users & sockets
const usersSockets = {};

//waiting list battle
const battleList = {};

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/build/index.html");
});

app.get("/send", function(req, res) {
  var sendMessage = new Message(
    req.query.user_emitter,
    req.query.user_receiver,
    req.query.message
  );
  messages.push(sendMessage);
});

server.listen(8080);

ioServer.on("connection", function(socket) {
  console.log("user connected to chat");

  socket.on("send-nickname", nickname => {
    console.log("try nickname " + nickname);
    if (nickname != null) {
      console.log("adding nickname " + nickname);
      //save socket and update if same
      usersSockets[nickname] = socket;
    }

    //Update user list

    var usersdata = {};

    for (var i in usersSockets) {
      if (usersSockets[i].connected) {
        if (usersSockets[i] != null) {
          usersdata[i] = usersSockets[i].connected;
        } else {
          usersdata[i] = false;
        }
      }
    }

    // console.log(usersdata);

    for (var i in usersSockets) {
      if (usersSockets[i] != null) {
        usersSockets[i].emit("update_users", usersdata);
      }
    }

    //Send messages
    var obj = [];
    obj[0] = messages.filter(msg => msg.emitterName == nickname);
    obj[1] = messages.filter(msg => msg.receiverName == nickname);
    //console.log(obj[0]);
    socket.emit("init", JSON.stringify(obj));
  });

  socket.on("play", nickname => {
    battleList[nickname] = socket;
    console.log("A user want to play", nickname);
    //console.log("battlelist",battleList[Object.keys(battleList)[Object.keys(battleList).length-1]]);
    //console.log("bnic",Object.keys(battleList));
    if (Object.keys(battleList).length % 2 == 0) {
      console.log("emit startPlay");
      usersSocket = getUsersBattleSockets();
      usersSocket[0].emit("startPlay", Object.keys(battleList));
      usersSocket[1].emit("startPlay", Object.keys(battleList));
    }
  });

  socket.on("message", msg => {
    console.log("received" + JSON.stringify(msg));
    //emittedMessage = new Message(msg.em,user_receiver,textMessage);
    messages.push(msg);

    stompit.connect(connectOptions, (error, client) => {
      if (error) {
        return console.error(error);
      }
      const frame = client.send(headers);
      frame.write(JSON.stringify(msg));
      frame.end();
      client.disconnect();
    });

    //get user socket
    socketReceiver = getUserSocket(msg.receiverName);

    console.log("Try to send message to " + msg.receiverName);
    //if receiver exists
    if (socketReceiver != null) {
      //send the message to his socket
      socketReceiver.emit("message", msg);

      //send message to backend spring
      //TODO active mq to springboot

      console.log("OK");
    } else {
      console.log("ERROR");
    }
  });
});

function getUserSocket(receiver) {
  socketReceiver = usersSockets[receiver];

  //if (socketReceiver == undefined) return null;
  return socketReceiver;
}

function getUsersBattleSockets() {
  return [
    battleList[Object.keys(battleList)[Object.keys(battleList).length - 1]],
    battleList[Object.keys(battleList)[Object.keys(battleList).length - 2]]
  ];
}
