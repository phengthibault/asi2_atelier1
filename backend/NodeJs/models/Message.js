
// class Message.js
module.exports = class Message {
    
    constructor({emitterName, receiverName, textMessage}){
        this.emitterName = emitterName;
        this.receiverName = receiverName;
        this.textMessage = textMessage;
        this.date = Date.now();
    }

    printMessage () {
        console.log('Message from ' + this.emiterName 
                        + ' to ' + this.receiverName);
    };

    error () {
        console.log("Can't send message to : " + this.receiverName)
    }
};
