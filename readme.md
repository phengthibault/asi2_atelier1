# Projet ASI2

## Membres du groupe 
1. Clément Landeroin
2. Paul Meallet
3. Antoine Crosetti
4. Thibault Pheng

## Github
Lien vers [GitHub](https://gitlab.com/phengthibault/asi2_atelier1)

## Youtube
https://www.youtube.com/watch?v=ftgmpTRqMt8

## Activitées réalisées par personne


### Clément Landeroin

- Découpage du front-end en composants.
- Codage du front end et de la partie React. Utilisation des ccomposants material-ui.
- Mise en place du store avec Redux et mise en place de Redux persist.
- Installation des routes avec React Rooter
- Conception de l'architecture globale avec le reste de l'équipe

### Meallet Paul 
- Atelier 1 : mettre à jour les propriétés d’un utilisateur par un BUS de communication
- Atelier 1 : application indépendante permettant de tester la mise à jour d’un utilisateur par Bus de Communication
- Atelier 2 : Réaliser un backend Node.js indépendant de l’existant permettant de créer un chat entre deux utilisateurs (javascript).
- Atelier 2 : Réaliser en BackEnd Node.js un service permettant d’associer les utilisateurs souhaitant jouer entre eux.
- Atelier 2 : Réaliser  en  un  diagramme  de  séquence du jeu
- Atelier 2 : Sauvegarde de l’historique des conversations du Chat en envoyant les données du BackEnd Node.js vers le BackEnd Springboot par buse de communication (Queue) pour stocker dans la db H2

### Antoine Crosetti 
- Back-end atelier 1
- Mise en place BUS de communication (springboot => springboot)
- Front-end Chat (react.js)
- Back-end Chat (node.js)
- Mise en place BUS de communication (node.js => springboot)

### Thibault Pheng
- Front-end react du login +enregistrement de l'utilisateur et la gameZone pour l'atelier 2
- Back end Springboot (en particulier modification/ajout dans le restController pour les cartes et l'user)
- communication du serveur node.js avec le front 
- Integration de l'ensemble chat+zone de jeu 

### How to run
- backend: Run in springboot folder  asi2-backendmarket-monolithic-student, UserModelPublic,Message
- run activemq start
- Frontend: npm install ensuite npm start in folder frontend/CardUserFront
- run nodeJs serveur in backend/NodeJs : npm start

## Liste des activitées réalisées

1. Atelier 1 : tout fait.
2. Atelier 2 : 
- Phase A : faite.
- Phase B : 2.1
- Phase C : 3.1
- Phase D : 4.2

## Liste des activitées non réalisées :
1. Atelier 2 :
- Phase B : 2.2, 2.3
- Phase C : 3.2
- Phase D : 4.1
