import React, { Component } from "react";
import { Provider } from "react-redux";
//import store from "./Store/store";
import CardContainer from "./Components/CardContainer";
import NavBar from "./Components/NavBar";
import LoginContainer from "./Components/LoginContainer";
import "./Semantic-UI-CSS-master/semantic.min.css";
import Home from "./Components/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./Store/store";
import Gamezone from "./Components/GameZone";
import LoadGameZone from "./Components/LoadGameZone";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <Switch>
              <Route exact path="/">
                <LoginContainer></LoginContainer>
              </Route>
              <Route exact path="/wait">
                <NavBar></NavBar>
                <LoadGameZone></LoadGameZone>
              </Route>
              <Route exact path="/home">
                <NavBar></NavBar>
                <Home></Home>
              </Route>
              <Route exact path="/card">
                <NavBar></NavBar>
                <CardContainer></CardContainer>
              </Route>
              <Route exact path="/game">
                <NavBar></NavBar>
                <Gamezone></Gamezone>
              </Route>
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
