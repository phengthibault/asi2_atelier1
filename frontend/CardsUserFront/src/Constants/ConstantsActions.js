export const GET_CARDS_TO_BUY = "GET_CARDS_TO_BUY";
export const GET_CARDS_TO_SELL = "GET_CARDS_TO_SELL";
export const SET_SELECTED_CARD = "SET_SELECTED_CARD";
export const SET_BUY_OR_SELL = "SET_BUY_OR_SELL";
export const SET_CARDS = "SET_CARDS";
export const SET_CARD_MODE = "SET_CARD_MODE";
export const SET_USER = "SET_USER";
export const SET_USERS = "SET_USERS";
