import {
  SET_SELECTED_CARD,
  SET_CARD_MODE,
  SET_USER,
  SET_USERS,
  SET_CARDS

} from "../Constants/ConstantsActions";

const initialState = {
  cards: [
    {
      id: 1,
      name: "fakecard1",
      description: "this is a fakecard",
      family: "fakecard",
      affinity: "aff",
      imgUrl: "https://picsum.photos/200/300",
      smallImgUrl: "url",
      energy: "fire",
      hp: "lp",
      defence: "def",
      attack: 100,
      price: 200
    },
    {
      id: 2,
      name: "fakecard2",
      description: "this is a fakecar2",
      family: "fakecard2",
      affinity: "aff2",
      imgUrl: "https://picsum.photos/200/300",
      smallImgUrl: "url2",
      energy: "fire2",
      hp: "lp2",
      defence: "def2",
      attack: 100,
      price: 200
    }
  ],
  selectedCard: {},
  cardMode: "home",
  user: {},
  users:{},
};

function cardsReducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case SET_SELECTED_CARD:
      console.log(action.payload);
      newState.selectedCard = action.payload;
      break;
    case SET_CARD_MODE:
      newState.cardMode = action.payload;
      break;
    case SET_USER:
      newState.user = action.payload;
      break;
    case SET_USERS:
        newState.users=action.payload;
      break;
    case SET_CARDS:
      newState.cards = action.payload;
      break;
    default:
      return newState;
  }
  return newState;
}
export default cardsReducer;
