import { createStore } from "redux";
import cardsReducer from "../Reducers/CardsReducer";
import { devToolsEnhancer } from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

const persistConfig = {
  key: "root",
  storage
};

const persistedReducer = persistReducer(persistConfig, cardsReducer);
//const store = createStore(persistedReducer, devToolsEnhancer());

export const store = createStore(persistedReducer,devToolsEnhancer());
export const persistor = persistStore(store);
