import { SET_SELECTED_CARD } from "../Constants/ConstantsActions";
export default function setSelectedCard(payload) {
  return { type: SET_SELECTED_CARD, payload };
}
