import { SET_CARDS } from "../Constants/ConstantsActions";
export default function setCards(payload) {
  return { type: SET_CARDS, payload };
}
