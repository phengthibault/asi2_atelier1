import { SET_BUY_OR_SELL } from "../Constants/ConstantsActions";
export default function setBuyOrSell(payload) {
  return { type: SET_BUY_OR_SELL, payload };
}
