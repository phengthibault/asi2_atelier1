import { SET_USERS } from "../Constants/ConstantsActions";
export default function setUsers(payload) {
  return { type: SET_USERS, payload };
}
