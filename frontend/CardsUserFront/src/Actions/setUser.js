import { SET_USER } from "../Constants/ConstantsActions";
export default function setUser(payload) {
  return { type: SET_USER, payload };
}
