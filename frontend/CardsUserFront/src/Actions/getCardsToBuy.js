import { GET_CARDS_TO_SELL } from "../Constants/ConstantsActions";
export function getCardsToBuy(payload) {
  return { type: GET_CARDS_TO_SELL, payload };
}
