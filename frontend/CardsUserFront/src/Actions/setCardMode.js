import { SET_CARD_MODE } from "../Constants/ConstantsActions";
export default function setCardMode(payload) {
  return { type: SET_CARD_MODE, payload };
}
