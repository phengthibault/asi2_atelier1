import React, { Component } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import { Table } from "@material-ui/core";
import BuyOrSellButton from "./BuyOrSellButton";

class DisplayCard extends Component {
  render() {
    return (
      <div>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <img
                    src={this.props.selectedCard.imgUrl}
                    width="200px"
                    height="300px"
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(this.props.selectedCard).map(key => (
                <TableRow>
                  <TableCell>
                    {key + " => " + this.props.selectedCard[key]}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>

        <BuyOrSellButton></BuyOrSellButton>
      </div>
    );
  }
}

export default DisplayCard;
