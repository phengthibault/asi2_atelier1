import React, { Component } from "react";
import HomeButton from "./HomeButton.js";

class Home extends Component {
  render() {
    return (
      <div>
        <HomeButton color="primary" value="Buy"></HomeButton>
        <HomeButton color="secondary" value="Sell"></HomeButton>
        <HomeButton color="danger" value="Play"></HomeButton>
      </div>
    );
  }
}

export default Home;
