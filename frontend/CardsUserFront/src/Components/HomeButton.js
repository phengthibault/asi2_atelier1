import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import setCardMode from "../Actions/setCardMode";
import { Link } from "react-router-dom";

function mapDispatchToProps(dispatch) {
  return {
    setCardMode: cardMode => dispatch(setCardMode(cardMode))
  };
}

class ConnectedHomeButton extends Component {
  whichMode(value) {
    switch (value) {
      case "Buy":
        return "buy";
      case "Sell":
        return "sell";
      case "Play":
        return "play";
    }
  }

  render() {
    return (
      <div>
        <Button
          variant="outlined"
          color={this.props.color}
          onClick={() =>
            this.props.setCardMode(this.whichMode(this.props.value))
          }
          component={Link}
          to={this.props.value==="Play"?"/wait":"/card"}   //route page
        >
          {this.props.value}
        </Button>
      </div>
    );
  }
}

const HomeButton = connect(
  null,
  mapDispatchToProps
)(ConnectedHomeButton);

export default HomeButton;
