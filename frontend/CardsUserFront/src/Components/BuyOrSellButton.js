import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { URLServer } from "../Constants/ConstantsUrl";
import setUser from "../Actions/setUser";
import setCards from "../Actions/setCards";
import setSelectedCard from "../Actions/setSelectedCard";

var axios = require("axios");

const mapStateToProps = state => {
  return {
    cardMode: state.cardMode,
    user: state.user,
    selectedCard: state.selectedCard
  };
};

function mapDispatchToProps(dispatch) {
  return {
    setUser: user => dispatch(setUser(user)),
    setCards: cards => dispatch(setCards(cards)),
    setSelectedCard: card => dispatch(setSelectedCard(card))
  };
}

class ConnectedBuyOrSellButton extends Component {
  constructor(props) {
    super(props);

    this.buyOrSellCard = this.buyOrSellCard.bind(this);
    this.reloadUser = this.reloadUser.bind(this);
    this.reloadCard = this.reloadCard.bind(this);
  }

  reloadUser(userId) {
    let that = this;
    fetch(URLServer + "/user/" + userId)
      .then(function(response) {
        return response.json();
      })
      .then(function(user) {
        that.props.setUser(user);
      });
  }

  reloadCard(userId) {
    let that = this;
    if (this.props.cardMode === "buy") {
      axios.get(URLServer + "cards").then(function(response) {
        that.props.setCards(response.data);
      });
    } else {
      that.props.setCards(that.props.user.cardList);
      that.props.setSelectedCard([]);
    }
  }

  buyOrSellCard(userId, cardId) {
    let that = this;
    axios
      .post(URLServer + this.props.cardMode, {
        user_id: userId,
        card_id: cardId
      })
      .then(function(response) {
        if (response.data === true) {
          console.log(response);
          that.reloadUser(that.props.user.id);
          that.reloadCard(that.props.user.id);
          that.props.cardMode === "buy"
            ?  axios({
              method :'post',
              url:URLServer+'buy',
            data: {
              user_id: that.props.user.id,
              card_id: that.props.selectedCard.id,          }})
              .then(function(response) {
                console.log(response.data)
                console.log("reussi");
                alert(
                  "Operation réussie! Vous venez d'acheter la carte " +
                    that.props.selectedCard.name)
              
              })
                
                
            : alert(
                "Operation réussie! Vous venez de vendre la carte " +
                  that.props.selectedCard.name
              );
        } else {
          that.props.cardMode === "buy"
            ? alert(
                "Vous n'avez pas assez d'argent! Il vous manque " +
                  (that.props.selectedCard.price - that.props.user.account)
              )
            : alert("Erreur dans la vente de cartes");
        }
      });
  }

  render() {
    return (
      <div>
        <Button
          onClick={() =>
            this.buyOrSellCard(this.props.user.id, this.props.selectedCard.id)
          }
          variant="contained"
        >
          {this.props.cardMode == "buy" ? "Buy" : "Sell"}
        </Button>
      </div>
    );
  }
}

const BuyOrSellButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedBuyOrSellButton);
export default BuyOrSellButton;
