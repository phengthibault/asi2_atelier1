import React, { Component } from "react";
import CardContainer from "./CardContainer";
import Home from "./Home";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { cardMode: state.cardMode };
};

class ConnectedAppContainer extends Component {
  render() {
    return (
      <div>
        {this.props.cardMode == "home" ? (
          <Home></Home>
        ) : (
          <CardContainer></CardContainer>
        )}
      </div>
    );
  }
}

const AppContainer = connect(mapStateToProps)(ConnectedAppContainer);

export default AppContainer;
