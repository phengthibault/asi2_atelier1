import React from "react";
import "../css/chat.css";
import io from "socket.io-client";
import { withRouter } from "react-router-dom";
import setUsers from "../Actions/setUsers";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    users: state.users
    // user: state.user,
  };
};
function mapDispatchToProps(dispatch) {
  return {
    setUsers: users => dispatch(setUsers(users))
  };
}

class ChatConnected extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: this.props.user, //"undefined",
      users: [
        /*"Antoine","Paul","Jojo"*/
      ],
      selectedUser: "",
      messages: [
        /*
          {

            emitterName : "Paul",
            receiverName : "Antoine",
            date : 2019,
            textMessage : "Salut pelo !",
            type : "message"

          },

          {

            emitterName : "Jojo",
            receiverName : "Antoine",
            date : 2018,
            textMessage : "Salut je suis jojo !",
            type : "message"

          },

          {

            emitterName : "Antoine",
            receiverName : "Paul",
            date : 2020,
            textMessage : "Salut Paulmea !",
            type : "reply"

          }*/
      ]
    };
  }
  componentDidMount() {
    let that = this;

    this.socket = io("http://localhost:8080/");
    console.log("utimisateur connecte:", this.state.currentUser);
    //this.state.currentUser = prompt("Please enter your name", "");
    this.state.users.push(this.state.currentUser, true);
    this.socket.emit("play", this.state.currentUser);
    this.socket.emit("send-nickname", this.state.currentUser);
    this.socket.on("startPlay", function(data) {
      //if there are 2 players start play

      //alert("on peut jouer");
      that.props.setUsers(data);
      that.props.history.push("/game");
    });

    this.socket.on(
      "update_users",

      function(data) {
        //Reply

        if (!data) {
          data = { DefaultUser: false };
        }

        this.setState(prevState => ({
          users: data
        }));
      }.bind(this)
    );

    this.socket.on(
      "init",

      function(data) {
        //Reply
        data = JSON.parse(data);
        if (!data) {
          data = [];
        }

        if (!data[0]) {
          data[0] = [];
        }

        if (!data[1]) {
          data[1] = [];
        }

        data[0].forEach(element => {
          element.type = "message";

          this.setState(prevState => ({
            messages: [...prevState.messages, element]
          }));
        });

        //Messages
        data[1].forEach(element => {
          element.type = "reply";

          this.setState(prevState => ({
            messages: [...prevState.messages, element]
          }));
        });

        this.changeSelect();
      }.bind(this)
    );

    this.socket.on(
      "message",
      function(data) {
        data.type = "reply";

        this.setState(prevState => ({
          messages: [...prevState.messages, data]
        }));
      }.bind(this)
    );

    var select = document.getElementById("userSelect");
    select.onchange = this.changeSelect.bind(this);
    var button = document.getElementById("button");
    button.onclick = this.sendMessage.bind(this);

    var all = document.getElementById("button");
    document.body.onkeypress = this.keypress.bind(this);

    this.changeSelect();

    this.scrollToBottom();
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };

  keypress(e) {
    //See notes about 'which' and 'key'
    if (e.keyCode == 13) {
      this.sendMessage();
      return false;
    }
  }

  changeSelect() {
    var select = document.getElementById("userSelect");
    if (select === null || select.options[select.selectedIndex] === undefined) {
      return;
    }
    let user = select.options[select.selectedIndex].value;

    this.setState({ selectedUser: user }); // modifie uniquement cette propriété
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  renderMessages() {
    let temp = [];
    var messages = this.state.messages;
    messages.sort((a, b) => parseFloat(a.date) - parseFloat(b.date));

    messages.forEach(element => {
      if (
        (element.emitterName === this.state.selectedUser &&
          element.receiverName === this.state.currentUser) ||
        (element.receiverName === this.state.selectedUser &&
          element.emitterName === this.state.currentUser)
      ) {
        temp.push(
          <div className={element.type}>
            <li className="clearfix">
              <div className="data align-right">
                <span className="data-name">{element.emitterName}</span>&nbsp;
                <span className="data-time">{this.getDate(element.date)}</span>
              </div>
              <div className="data-message">{element.textMessage}</div>
            </li>
          </div>
        );
      }
    });

    return temp;
  }

  getDate(unix_timestamp) {
    var date = new Date(unix_timestamp);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp

    // Will display time in 10:30:23 format
    return hours + ":" + minutes.substr(-2);
  }

  renderSelect() {
    let temp = [];

    for (var i in this.state.users) {
      console.log(i);
      if (i !== this.state.currentUser) {
        //console.log(this.state.users[i]);
        if (this.state.users[i]) {
          temp.push(
            <option className="online" value={i}>
              (Online) {i}
            </option>
          );
        } else {
          temp.push(
            <option className="offline" value={i}>
              {i}
            </option>
          );
        }
      }
    }

    return temp;
  }

  sendMessage() {
    this.changeSelect();
    var txt = document.getElementById("textbox").value;
    var message = {
      emitterName: this.state.currentUser,
      receiverName: this.state.selectedUser,
      date: Date.now(),
      textMessage: txt
    };

    this.socket.emit("message", message);

    message.type = "message";

    this.setState(prevState => ({
      messages: [...prevState.messages, message]
    }));

    document.getElementById("textbox").value = "";
  }

  render() {
    return (
      <div className="messagebox">
        <select id="userSelect">{this.renderSelect()}</select>

        <div className="messages" id="Antoine">
          {this.renderMessages()}

          <div
            style={{ float: "left", clear: "both" }}
            ref={el => {
              this.messagesEnd = el;
            }}
          ></div>
        </div>

        <div className="replybox">
          <input
            className="textbox"
            id="textbox"
            type="text"
            name="fname"
          ></input>
          <button className="button" id="button" type="button">
            Send
          </button>
        </div>
      </div>
    );
  }
}

//export default withRouter(Chat);
const Chat = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatConnected);

export default withRouter(Chat);
