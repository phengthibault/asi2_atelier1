import React, { Component } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import setSelectedCard from "../Actions/setSelectedCard";
import { connect } from "react-redux";

function mapDispatchToProps(dispatch) {
  return {
    setSelectedCard: selectedCard => dispatch(setSelectedCard(selectedCard))
  };
}

class DisplayCardGame extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let arr_render = [];

    switch (this.props.cardTypeDisplay) {
      case "CARD_LONG_DISPLAY":
        arr_render.push(
          <div className="ui special cards">
            <div className="card">
              <div className="content">
                <div className="ui grid">
                  <div className="three column row">
                    <div className="column">
                      <i className="heart outline icon"></i>
                      <span id="cardHPId">{this.props.card.hp}</span>
                    </div>
                    <div className="column">
                      <h5>{this.props.card.name}</h5>
                    </div>
                    <div className="column">
                      <span id="energyId">{this.props.card.energy}</span>{" "}
                      <i className="lightning icon"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div className="image imageCard">
                <div className="blurring dimmable image">
                  <div className="ui inverted dimmer">
                    <div className="content">
                      <div className="center">
                        <div className="ui primary button">Add Friend</div>
                      </div>
                    </div>
                  </div>
                  <div className="ui fluid image">
                    <a className="ui left corner label">
                      {this.props.card.name}
                    </a>
                    <img
                      id="cardImgId"
                      className="ui centered image"
                      src={this.props.card.imgUrl}
                    />
                  </div>
                </div>
              </div>
              <div className="content">
                <div className="ui form tiny">
                  <div className="field">
                    <label id="cardNameId"></label>
                    <textarea
                      id="cardDescriptionId"
                      className="overflowHiden"
                      readOnly=""
                      rows="2"
                      defaultValue="I love ASI2 especially during the weekend"
                    />
                  </div>
                </div>
              </div>
              <div className="content">
                <i className="heart outline icon"></i>
                <span id="cardHPId"> HP {this.props.card.hp}</span>
                <div className="right floated ">
                  <span id="cardEnergyId">Energy {this.props.card.energy}</span>
                  <i className="lightning icon"></i>
                </div>
              </div>
              <div className="content">
                <span className="right floated">
                  <span id="cardAttackId">
                    {" "}
                    Attack {this.props.card.attack}
                  </span>
                  <i className=" wizard icon"></i>
                </span>
                <i className="protect icon"></i>
                <span id="cardDefenceId">
                  Defense {this.props.card.defence}
                </span>
              </div>
              <div className="ui bottom attached button">
                <i className="money icon"></i>
                Actual Value{" "}
                <span id="cardPriceId"> {this.props.card.price} $</span>
              </div>
            </div>
          </div>
        );
        break;
      case "CARD_SHORT_DISPLAY":
        arr_render.push(
          <div className="ui special cards">
            <div className="card">
              <div className="content">
                <div className="ui grid">
                  <div className="three column row">
                    <div className="column">
                      <a className="ui red circular label">
                        {this.props.card.hp}
                      </a>
                    </div>
                    <div className="column">
                      <h5>{this.props.card.name}</h5>
                    </div>
                    <div className="column">
                      <a className="ui yellow circular label">
                        {this.props.card.attack}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="image imageCard">
                <div className="ui fluid image">
                  <img
                    id="cardImgId"
                    className="ui centered image"
                    src={this.props.card.imgUrl}
                  />
                </div>
              </div>
            </div>
          </div>
        );
        break;
      default:
        console.log("type not defined");
    }
    return arr_render;
  }
}

const DisplayLine = connect(
  null,
  mapDispatchToProps
)(DisplayCardGame);

export default DisplayCardGame;
