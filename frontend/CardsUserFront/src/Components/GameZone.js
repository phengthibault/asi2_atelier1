import React, { Component } from "react";
import "../Semantic-UI-CSS-master/semantic.min.css";
import { connect } from "react-redux";
import DisplayCardGame from "./DisplayCardGame";
import Chat from "./Chat";
import axios from "axios";
import setCards from "../Actions/setCards";
import setSelectedCard from "../Actions/setSelectedCard";

const mapStateToProps = state => {
  return {
    cardMode: state.cardMode,
    user: state.user,
    selectedCard: state.selectedCard,
    cards: state.cards,
    users: state.users
  };
};

function mapDispatchToProps(dispatch) {
  return {
    setCards: cards => dispatch(setCards(cards)),
    setSelectedCard: selectedCard => dispatch(setSelectedCard(selectedCard))
  };
}

class GameZoneConnect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardSelectedA: [],
      cardSelectedB: [],
      cardsUser1: [],
      cardsUser2: []
    };
    this.DisplayCardsOnGame = this.DisplayCardsOnGame.bind(this);
    this.DisplayCardsOfUser = this.DisplayCardsOfUser.bind(this);
    this.DisplayCardSelected = this.DisplayCardSelected.bind(this);
  }

  async componentDidMount() {
    let data = await this.DisplayCardsOfUser(this.props.users[0]);
    this.setState({ cardsUser1: data });
    data = await this.DisplayCardsOfUser(this.props.users[1]);
    this.setState({ cardsUser2: data });
    console.log("this.state", this.state);
  }

  DisplayCardsOfUser(userLogin) {
    console.log("on veut les cartes de+", userLogin);
    return axios
      .get(
        "http://localhost:8082/userCardslog/" + userLogin //this.props.user
      )
      .then(response => {
        console.log("on arecu les carte de l'user" + response);
        return response.data;
      });
  }

  DisplayCardsOnGame(cards, player) {
    let array_render = [];
    cards.map(e => {
      array_render.push(
        <div className="column">
          <div
            id={e.id}
            key={e.id}
            onClick={() => {
              player === "A"
                ? this.setState({ cardSelectedA: e })
                : this.setState({ cardSelectedB: e });
            }}
          >
            <DisplayCardGame
              key={e.id}
              card={e}
              cardTypeDisplay="CARD_SHORT_DISPLAY"
            />
          </div>
        </div>
      );
    });
    return array_render;
  }

  DisplayCardSelected(cardSelected) {
    let array_render = [];
    //console.log("carte selectionné+", card);
    array_render.push(
      <DisplayCardGame
        key={cardSelected.id}
        card={cardSelected}
        cardTypeDisplay="CARD_LONG_DISPLAY"
      />
    );

    return array_render;
  }

  render() {
    var listCardsOnGameTop;
    var listCardsOnGameBot;
    var cardUser1;
    if (this.props.users.length >= 2) {
      console.log("les users enregistre+", this.props.users);
      var nameUser1 = this.props.users[0];
      var nameUser2 = this.props.users[1];
      console.log(this.props.users[0]);
      console.log(this.props.users[1]);
      console.log("retour carteuser1", cardUser1);
      listCardsOnGameTop = this.DisplayCardsOnGame(this.state.cardsUser1, "A");
      //var cardUser2=this. DisplayCardsOfUser(this.props.users[1]);
      //console.log("retour carteuser1",cardUser2);

      listCardsOnGameBot = this.DisplayCardsOnGame(this.state.cardsUser2, "B");
      var DisplaySelectedCardTop = this.DisplayCardSelected(
        this.state.cardSelectedA
      );
      var DisplaySelectedCardBot = this.DisplayCardSelected(
        this.state.cardSelectedB
      );
    }
    return (
      <div>
        <div className="ui segment">
          <div className="ui grid">
            <div className="three wide column">
              <div id="chatContent">
                <Chat user={this.props.user.login}></Chat>
              </div>
            </div>
            <div className="twelve wide column">
              <div className="row">
                <div className="ui grid">
                  <div className="two wide column">
                    <div className="ui one  column centered grid">
                      <div className="row">
                        <div className="column">
                          {" "}
                          <i className="user circle huge icon "></i>
                        </div>
                      </div>
                      <div className="row">
                        <div className=" column">{nameUser2}</div>
                      </div>

                      <div className="row">
                        <div className="column">
                          <div
                            className="ui teal progress"
                            data-percent="74"
                            id="progressBarId1"
                          >
                            <div className="bar"></div>
                            <div className="label">Action Points</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ten wide column">
                    <div className="ui four column grid">
                      {listCardsOnGameTop}
                    </div>
                  </div>
                  <div className="four wide column">
                    {DisplaySelectedCardTop}
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="ui grid ">
                  <div className="four wide column">
                    <button className="huge ui primary button">End Turn</button>
                  </div>
                  <div className="eight wide column">
                    <h4 className="ui horizontal divider header">VS</h4>
                  </div>
                  <div className="four wide column">
                    <button className="huge ui primary button">Attack</button>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="ui grid">
                  <div className="two wide column">
                    <div className="ui one  column centered grid">
                      <div className="row">
                        <div className="column">
                          <div
                            className="ui teal progress"
                            data-percent="20"
                            id="progressBarId2"
                          >
                            <div className="label">Action Points</div>
                            <div className="bar"></div>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className=" column">{nameUser1}</div>
                      </div>
                      <div className="row">
                        <div className="column">
                          {" "}
                          <i className="user circle huge icon "></i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ten wide column">
                    <div className="ui four column grid">
                      {listCardsOnGameBot}
                    </div>
                  </div>
                  <div className="four wide column">
                    <div id="fullCardB1"> {DisplaySelectedCardBot}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

//const GameZoneconnect = connect(mapStateToProps)(GameZone);

const GameZone = connect(
  mapStateToProps,
  mapDispatchToProps
)(GameZoneConnect);

export default GameZone;
