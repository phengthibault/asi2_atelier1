import React, { Component } from "react";
import User from "./User";
import UserForm from "./UserForm";
import LoginPage from "./LoginPage";
import Button from "@material-ui/core/Button";

class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DisplayLogin: false,
      DisplayRegister: false,
      showMe: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitUserHandler = this.submitUserHandler.bind(this);
    this.RegisterDisplay = this.RegisterDisplay.bind(this);
    this.LoginDisplay = this.LoginDisplay.bind(this);
  }

  callbackErr(data) {
    console.log(data);
  }

  handleChange(data) {
    console.log(data);
    this.setState({
      currentUser_id: data.id,
      currentUser_surname: data.surName,
      currentUser_lastname: data.lastName,
      currentUser_login: data.login,
      currentUser_pwd: data.pwd,
      currentUser_money: data.account,
      currentUser_img: data.img
    });
  }

  submitUserHandler(data) {
    console.log("user to submit" + data);
  }

  RegisterDisplay() {
    this.setState({
      DisplayRegister: true,
      DisplayLogin: false
    });
  }

  LoginDisplay() {
    this.setState({
      DisplayLogin: true,
      DisplayRegister: false
    });
  }

  //render function use to update the virtual dom
  render() {
    let Display = [];

    Display.push(
      <div
        className="btn-group"
        style={{
          position: "absolute",
          top: "30%",
          left: "45%",
          display:
            this.state.DisplayLogin || this.state.DisplayRegister
              ? "none"
              : "block"
        }}
      >
        <Button
          variant="outlined"
          color="primary"
          onClick={this.RegisterDisplay}
        >
          Register{" "}
        </Button>
        <Button variant="outlined" color="primary" onClick={this.LoginDisplay}>
          Log in{" "}
        </Button>
      </div>
    );

    if (this.state.DisplayLogin) {
      Display.push(
        <div className="Login">
          <LoginPage DisplayRegister={this.DisplayRegister} />
        </div>
      );
    }

    if (this.state.DisplayRegister) {
      Display.push(
        <div className="ui three column grid">
          <div className="middle aligned row">
            <div className="column">
              <div className="ui segment">
                <UserForm
                  handleChange={this.handleChange}
                  submitUserHandler={this.submitUserHandler}
                  LoginDisplay={this.LoginDisplay}
                ></UserForm>
              </div>
            </div>
            <div className="column">
              <div className="ui one centered column grid">
                <User
                  id={this.state.currentUser_id}
                  surName={this.state.currentUser_surname}
                  lastName={this.state.currentUser_lastname}
                  login={this.state.currentUser_login}
                  pwd={this.state.currentUser_pwd}
                  money={this.state.currentUser_money}
                  img={this.state.currentUser_img}
                  display_type="FULL"
                ></User>
              </div>
            </div>
            <div className="column">
              <div className="ui one centered column grid">
                <User
                  id={this.state.currentUser_id}
                  surName={this.state.currentUser_surname}
                  lastName={this.state.currentUser_lastname}
                  login={this.state.currentUser_login}
                  pwd={this.state.currentUser_pwd}
                  money={this.state.currentUser_money}
                  img={this.state.currentUser_img}
                  display_type="SHORT"
                ></User>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return Display;
  }
}

export default LoginContainer;
