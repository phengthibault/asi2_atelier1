import React from "react";

class UserSimpleDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      surname: this.props.surname,
      lastname: this.props.lastname,
      login: this.props.login,
      pwd: this.props.pwd,
      money: this.props.money,
      img: this.props.img
    };
  }
  render() {
    return (
      <div className="ui card">
        <div
          className="image"
          style={{
            display:
              this.props.img === "" || this.props.img === undefined
                ? "none"
                : "block"
          }}
        >
          <img src={this.props.img} alt="avatar"></img>
        </div>
        <div className="content">
          <a className="header">
            {this.props.surName} {this.props.lastName}
          </a>
          <div className="meta">
            <span className="date">login: {this.props.login}</span>
          </div>
        </div>
        <div className="extra content">
          <a>
            <i className="money bill alternate outline icon"></i>
            {this.props.money} $
          </a>
        </div>
      </div>
    );
  }
}
export default UserSimpleDisplay;
