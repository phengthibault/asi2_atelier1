import React, { Component } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import setSelectedCard from "../Actions/setSelectedCard";
import { connect } from "react-redux";

function mapDispatchToProps(dispatch) {
  return {
    setSelectedCard: selectedCard => dispatch(setSelectedCard(selectedCard))
  };
}

class ConnectedDisplayLine extends Component {
  render() {
    return (
      <TableRow
        onClick={() => this.props.setSelectedCard(this.props.cardProperties)}
        hover="true"
        key={this.props.cardProperties.id}
      >
        {Object.values(this.props.cardProperties).map(item => {
          return <TableCell align="right">{item}</TableCell>;
        })}
      </TableRow>
    );
  }
}
const DisplayLine = connect(
  null,
  mapDispatchToProps
)(ConnectedDisplayLine);

export default DisplayLine;
