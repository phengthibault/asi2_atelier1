import React from "react";

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      LoginDisplay: this.props.LoginDisplay,
      handleChange: this.props.handleChange,
      submitUserHandler: this.props.submitUserHandler,
      surName: "",
      lastName: "",
      img: "",
      login: "",
      pwd: "",
      account: 0
    };

    this.processInput = this.processInput.bind(this);
  }

  processInput(event) {
    const target = event.currentTarget;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    console.log(event.target.value);
    let currentVal = this.state;
    this.setState({
      [name]: value
    });
    currentVal[name] = value;
    console.log(name, value);
    this.props.handleChange(currentVal);
  }

  submitOrder(data) {
    const url = "http://localhost:8082/user";
    console.log("submit");

    console.log(this.state);
    const response = fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(this.state)
    }).then(response => {
      if (response.ok) {
        alert("utilisateur ajouter :" + this.state.surName);
        this.props.LoginDisplay();
      } else {
        response.json().then(err => Promise.reject(err));
        alert("error", response);
      }
    });

    this.props.submitUserHandler(JSON.stringify(this.state));
  }

  render() {
    return (
      <form enctype="application/json" className="ui form">
        <h4 className="ui dividing header">User Registration</h4>

        <div className="field">
          <label>Name</label>
          <div className="two fields">
            <div className="field">
              <input
                className="text"
                name="surName"
                placeholder="SurName"
                onChange={ev => {
                  this.processInput(ev);
                }}
                value={this.state.surName}
              ></input>
            </div>
            <div className="field">
              <input
                className="text"
                name="lastName"
                placeholder="LastName"
                onChange={ev => {
                  this.processInput(ev);
                }}
                value={this.state.lastName}
              ></input>
            </div>
          </div>
        </div>
        <div className="field">
          <label>Login</label>
          <input
            type="text"
            name="login"
            placeholder="Login"
            onChange={ev => {
              this.processInput(ev);
            }}
            value={this.state.login}
          ></input>
        </div>
        <div className="field">
          <label>Pwd</label>
          <input
            type="password"
            name="pwd"
            placeholder=""
            onChange={ev => {
              this.processInput(ev);
            }}
            value={this.state.pwd}
          ></input>
        </div>
        <div className="field">
          <label>Image</label>
          <input
            type="text"
            name="img"
            placeholder="Image"
            onChange={ev => {
              this.processInput(ev);
            }}
            value={this.state.img}
          ></input>
        </div>
        <div className="field">
          <label>Money</label>
          <input
            type="number"
            name="account"
            placeholder=""
            onChange={ev => {
              this.processInput(ev);
            }}
            value={this.state.account}
          ></input>
        </div>
        <div
          className="ui button"
          tabIndex="1"
          onClick={data => this.submitOrder(data)}
        >
          Submit User
        </div>
      </form>
    );
  }
}
export default UserForm;
