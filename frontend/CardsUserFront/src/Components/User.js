import React from "react";
import UserSimpleDisplay from "./UserSimpleDisplay";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { user: state.user };
};

class ConnectedUser extends React.Component {
  FULL_LABEL = "FULL";
  SHORT_LABEL = "SHORT";
  MONEY_LABEL = "MONEY";
  PICTURE_LABEL = "PICTURE";

  render() {
    let display = "";
    switch (this.props.display_type) {
      case this.FULL_LABEL:
        display = (
          <UserSimpleDisplay
            id={this.props.id}
            surName={this.props.surName}
            lastName={this.props.lastName}
            login={this.props.login}
            pwd={this.props.pwd}
            money={this.props.money}
            img={this.props.img}
          ></UserSimpleDisplay>
        );
        break;
      case this.MONEY_LABEL:
        display = (
          <div style={this.props.style}>
            <h2>{this.props.user.account} $</h2>
          </div>
        );
        break;
      case this.PICTURE_LABEL:
        display = (
          <div style={{ ...this.props.style, textAlign: "right" }}>
            <h2>{this.props.user.surName + " " + this.props.user.lastName}</h2>
          </div>
        );
    }
    return display;
  }
}

const User = connect(mapStateToProps)(ConnectedUser);

export default User;
