import React, { Component } from "react";
import Card from "./Card";
import setCards from "../Actions/setCards";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import { connect } from "react-redux";
import Axios from "axios";

const mapStateToProps = state => {
  return { cards: state.cards, cardMode: state.cardMode, user: state.user };
};

function mapDispatchToProps(dispatch) {
  return {
    setCards: cards => dispatch(setCards(cards))
  };
}

class ConnectedCardTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlBuy: "http://localhost:8082/cards",
      urlSell: "http://localhost:8082/userCards/" + this.props.user.id
    };
  }

  componentDidMount() {
    let that = this;
    Axios.get(
      this.props.cardMode === "buy" ? this.state.urlBuy : this.state.urlSell
    ).then(function(response) {
      that.props.setCards(response.data);
    });
  }

  getTableHeadValues() {
    if (this.props.cards.length !== 0) {
      return Object.keys(this.props.cards[0]);
    } else {
      return [];
    }
  }

  render() {
    return (
      <div>
        <Paper
          style={{
            width: "100%",
            marginTop: "3px",
            overflowX: "auto"
          }}
        >
          <Table>
            <TableHead>
              <TableRow>
                {this.getTableHeadValues().map(title => (
                  <TableCell align="right">{title}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {this.props.cards.map(cardProperties => (
                <Card display="table" cardProperties={cardProperties}></Card>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }
}

const CardTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedCardTable);

export default CardTable;
