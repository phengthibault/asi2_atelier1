import React, { Component } from "react";
import CardTable from "./CardTable";
import Card from "./Card";

class CardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      frameStyle: { display: "inline-block" }
    };
  }
  render() {
    return (
      <div>
        <div
          style={{
            width: "78%",
            overflow: "auto",
            display: "inline-block",
            "vertical-align": "top",
            padding: "5px"
          }}
        >
          <CardTable></CardTable>
        </div>
        <div
          style={{
            width: "20%",
            display: "inline-block",
            float: "right"
          }}
        >
          <Card display="card"></Card>
        </div>
      </div>
    );
  }
}

export default CardContainer;
