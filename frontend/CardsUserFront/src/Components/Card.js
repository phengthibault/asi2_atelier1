import React, { Component } from "react";
import { connect } from "react-redux";
import DisplayLine from "./DisplayLine";
import DisplayCard from "./DisplayCard";
import GameZone from "./GameZone";
const mapStateToProps = state => {
  return { selectedCard: state.selectedCard, user: state.user };
};

class ConnectedCard extends Component {
  inlineMode() {
    return (
      <DisplayLine cardProperties={this.props.cardProperties}></DisplayLine>
    );
  }

  cardMode() {
    if (Object.keys(this.props.selectedCard).length !== 0) {
      return <DisplayCard selectedCard={this.props.selectedCard}></DisplayCard>;
    } else {
      return <div></div>;
    }
  }
  gameMode() {
    return <GameZone></GameZone>;
  }

  whichMode() {
    switch (this.props.display) {
      case "table":
        return this.inlineMode();
      case "card":
        return this.cardMode();
      case "play":
        return this.gameMode();
      default:
        return;
    }
  }

  render() {
    return this.whichMode();
  }
}

const Card = connect(mapStateToProps)(ConnectedCard);

export default Card;
