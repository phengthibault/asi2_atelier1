import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import User from "./User";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "padding-bottom": "5px"
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

export default function ButtonAppBar() {
  const classes = useStyles();

  const MONEY_LABEL = "MONEY";
  const PICTURE_LABEL = "PICTURE";

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar style={{ display: "flex" }}>
          <User style={{ flexGrow: 1 }} display_type={MONEY_LABEL}></User>
          <User style={{ flexGrow: 1 }} display_type={PICTURE_LABEL}></User>
        </Toolbar>
      </AppBar>
    </div>
  );
}
